# phanidark theme

developing a dark theme that i like to use. started off from redbasic's 'dark' schema, but it's not a fork.

this theme has two schemas: 'dark' and 'multicolumn.'

'dark' limits the width of page content to 700px, which i find more comfortable than the very wide text column that results from today's widescreen monitors. that column is centered in the page, no matter how wide it is.

changed the original colors a little, providing some differentiation between page background, the actual post, and comments.

'multicolumn' arranges the content of /network and /pubstream in two columns by floating each unit of post & comment to the left. column width depends on the available screen space which is increased by showing the content of the right side-bar before that of the left one, on the left side, right sidebar getting freed for content.

this fits more posts into each page, depending on number and size of comments, and what max. height is set for your stream page. isn't ideal because, depending on these factors, a lot of screen space can be wasted.

i don't want to hide comments though, which would make it possible to apply an equal height to each post & comment unit and make the whole thing look much neater. but i WANT to see the comments, so i'm living with it.

posts on your channel home or in single post display, after clicking on a notification per ex., are displayed in centered single column view.

at present it works for both, master and dev, but this may change when the respective core branches change. if you run it on master, go to /extend/theme/phanidark and run <code>git checkout master</code>

if on dev, do the same with <code>git checkout dev</code>, just to make sure the correct branch is active.


to install it go to the hub's webroot and issue the following command:

<code>util/add_theme_repo https://github.com/phani00/phanidark.git phanidark</code>
